/**
 * Events utils
 */
jQuery.fn.extend({
    getEventDay: function (eventDate) {

        return eventDate.substring(eventDate.indexOf('/') + 1, eventDate.lastIndexOf('/'));

    },
    getEventMonth: function (eventDate) {

        return eventDate.substring(0, eventDate.indexOf('/'));

    },
    getEventMonthString: function (eventDate) {

        const monthsArray = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ];

        return monthsArray[this.getEventMonth(eventDate) - 1];

    },
    getEventYear: function (eventDate) {

        return eventDate.substring(eventDate.length - 4, eventDate.length);

    },
    convertEventDate: function (eventDate) {

        return new Date(this.getEventYear(eventDate), this.getEventMonth(eventDate) - 1, this.getEventDay(eventDate));

    },
    isUpcomingEvent: function (eventDate) {

        const now = new Date;

        return (now < this.convertEventDate(eventDate));

    },
    eventProximity: function () {

        return {
            'is-upcoming': 'Upcoming Event',
            'past-event': 'Past Event'
        };

    },
    sortEventsByDate: function (data) {

        return data.sort((obj1, obj2) => {

            return this.convertEventDate(obj2.dateStart) - this.convertEventDate(obj1.dateStart);

        });

    },
    cleanEventsJSON: function (data) {

        for (let i = 0; i < data.length; i++) {

            if (!data[i].title ||
                !data[i].description ||
                !data[i].dateStart ||
                !data[i].url ||
                !data[i].image) {

                    data.splice(i, 1);

                }

        }

        return data;

    }
});
