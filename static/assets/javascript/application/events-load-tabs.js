/**
 * Load events' tabs
 */
jQuery.fn.extend({
    loadEventsTabs: function(data) {

        let eventsList = $('.js-filtered-events-list');

        if (!eventsList) return;

        let htmlTemplate = '';
        let eventUpocomingStatus = '';

        for (let cont = 0; cont < data.length; cont++) {

            // Check if upcoming or past event
            ($.fn.isUpcomingEvent(data[cont].dateStart)) ? eventUpocomingStatus = Object.keys($.fn.eventProximity())[0] : eventUpocomingStatus = Object.keys($.fn.eventProximity())[1];

            htmlTemplate += `
            <a
                class="filtered-event-item js-filtered-event-item"
                data-event-category="${ eventUpocomingStatus }"
                href="${ data[cont].url }"
                target="_blank"
            >
                <span class="filtered-event-image-container">
                    <span
                        class="filtered-event-image content-ratio"
                        style="background-image: url('${ data[cont].image }');"
                    ></span>
                </span>
                <span class="filtered-event-title">
                    <span class="filtered-event-date">
                        <span class="the-number">${ $.fn.getEventDay(data[cont].dateStart) }</span>
                        <span class="the-month">${ $.fn.getEventMonthString(data[cont].dateStart) }</span>
                    </span>
                    <span class="filtered-event-title-text">
                        <span class="filtered-event-title-name">${ data[cont].title }</span>
                        <span class="filtered-event-title-date ${ eventUpocomingStatus }">${ $.fn.eventProximity()[eventUpocomingStatus] }</span>
                    </span>
                </span>
            </a>`;
        }

        eventsList.html('');
        eventsList.append(htmlTemplate);
        this.removeEventsTabsLoading();

    },
    removeEventsTabsLoading: function() {

        $('.js-filtered-events-list-loading').addClass('hidden');
        $('.js-filtered-events-list').removeClass('hidden');

    }
});
