/**
 * Show video
 */
$(function () {

  let showBridgeVideo = $('.js-show-bridge-video');

  showBridgeVideo.on("click", function () {

    swal({
      confirmButtonColor: '#22d89e',
      confirmButtonText: "Close",
      html: '<iframe style="max-width: 100%; margin-top: -10px;" width="560" height="315" src="https://www.youtube.com/embed/OstGQQHgCt4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
      width: 600
    });

  });


});
