/**
 * Open news links on click
 */
$(function () {
    'use strict'

    let newsContainer = $('.js-network-news-section-news-list');

    if (!newsContainer.length) {

        return;

    }

    /**
     * Loads the highlighted news item
     * @param {*} data
     */
    let loadHighlightedNews = function(data) {

        let highlightedNewsContainer = $('.js-network-news-section-top-details');
        const info = data.filter((item) => {
            return item.highlighted === true;
        });

        const htmlTemplate = `
            <span
                class="the-image"
                style="background-image: url('${ info[0].image }')"
            ></span>
            <span class="the-info">
                <span class="the-title">${ info[0].title }</span>
                <span class="the-description">${ info[0].description }</span>
                <span
                    class="view-more"
                >Find out more</span>
            </span>
        `;

        highlightedNewsContainer.html('');
        highlightedNewsContainer.append(htmlTemplate);
        highlightedNewsContainer.attr({ 'href': info[0].url });

    };

    /**
     * Loads the regular news items
     * @param {*} data
     */
    let loadRegularNews = function(data) {

        let regularNewsContainer = $('.js-network-news-section-news-list');
        let htmlTemplate = '';
        const info = data.filter((item) => {
            return !item.highlighted;
        });

        for (let cont = 0; cont < info.length; cont++) {

            htmlTemplate += `
                <a href="${ info[cont].url }" class="network-news-section-news-list-item js-network-news-section-news-list-item">
                    <span
                        class="the-image"
                        style="background-image: url('${ info[cont].image }'); background-position:${ info[cont].position }"
                    ></span>
                    <span class="the-info">
                        <span class="the-title">${ info[cont].title }</span>
                        <span class="the-description">${ info[cont].description }</span>
                        <span
                            class="view-more"
                        >Find out more</span>
                    </span>
                </a>`;
        }

        regularNewsContainer.html('');
        regularNewsContainer.append(htmlTemplate);

    };

    /**
     * Initialize news section
     * @param {} data
     */
    let initialize = function(data) {

        loadHighlightedNews(data);
        loadRegularNews(data);
        removeLoading();

    };

    /**
     * Stop showing the loading thing
     */
    let removeLoading = function() {

        $('.js-network-news-section-loading').addClass('hidden');
        $('.js-network-news-section-content').removeClass('hidden');

    }

    /**
     * Get data from JSON file
     */
    let getNewsData = async () => {

        try {

            const response = await fetch('assets/JSON/news.json');

            initialize(await response.json());

          } catch (err) {

            console.log('Error fetching news JSON file.')

          }

    };

    getNewsData();

});
