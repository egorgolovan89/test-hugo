/**
 * Conversion blocks
 */
$(function () {

    "use strict"; // Start of use strict

    let conversionBlock = $('.js-xdai-conversion-blocks');

    if (!conversionBlock.length) {

        return;

    }

    let blockLoopArray = [
        '.xdai',
        '.poa',
        '.eth',
        '.poa'
    ];

    const animateBlocks = () => {
        let currentActiveBlock = 1;

        setInterval(() => {

            $('.js-xdai-conversion-info-block').removeClass('active');

            $('.js-xdai-conversion-info-block' + blockLoopArray[currentActiveBlock]).addClass('active');

            if(currentActiveBlock == (blockLoopArray.length - 1)) {
                currentActiveBlock = 0;
            }
            else {
                currentActiveBlock++;
            }

        }, 5000);
    }

    const initialize = () => {

        animateBlocks();

    }

    initialize();

});