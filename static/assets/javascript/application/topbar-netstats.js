/**
 * Topbar information
 */
$(function () {

    'use strict'

    const topbarContainer = $('.js-tns');

    if (!topbarContainer.length) {

        return;

    }

    const apiDelay = 5000;
    const defaultNetwork = 'core';
    const localStorage = window.localStorage;
    const networks = {
        core: {
            url: 'https://blockscout.com/poa/core/',
            apiURL: 'https://tuo2uaw74i.execute-api.us-east-1.amazonaws.com/POA/blockInfo',
            networkName: 'POA',
            explorerURL: 'https://blockscout.com/poa/core/',
            networkTitle: 'POA Core Chain',
            tag: 'core'
        },
        xdai: {
            url: 'https://blockscout.com/poa/dai/',
            apiURL: 'https://tuo2uaw74i.execute-api.us-east-1.amazonaws.com/POA/blockInfoXDai',
            networkName: 'Crosschain POA',
            explorerURL: 'https://blockscout.com/poa/dai/',
            networkTitle: 'xDai Stable Chain',
            tag: 'xdai'
        }
    };
    let networkDataInterval = undefined;
    let statsContainer;
    let currentNetworkTitleItem;
    let currentNetworkNameItem;
    let tnsItems;
    let blockItem;
    let lastBlockItem;
    let avgBlockTimeItem;
    let explorerButton;
    let changeNetworkButton;
    let networksModal;
    let networksModalClose;
    let networksModalNetworkItem;

    /**
     * Loads the topbar HTML template
     * @param {*} network
     */
    let loadNetworksModal = () => {

        const currentNetwork = getCurrentNetwork();
        let htmlTemplate = '';
        let htmlNetworksTemplate = '';

        Object.keys(networks).map((item, index) => {

            htmlNetworksTemplate += `
                <div data-network-tag="${ networks[item].tag }" class="js-tns-network tns-selector-modal-network tns-selector-modal-network-${ networks[item].tag }">
                    <div class="js-tns-network-check tns-selector-modal-network-check ${ currentNetwork === networks[item].tag ? 'tns-selector-modal-network-check-active' : '' }"></div>
                    <div class="tns-selector-modal-network-logo tns-selector-modal-network-logo-${ networks[item].tag }"></div>
                    <h3 class="tns-selector-modal-network-title">${ networks[item].networkTitle }</h3>
                </div>
            `;

        });

        htmlTemplate = `
            <div class="js-tns-modal tns-selector-modal hidden">
                <div class="tns-selector-modal-container">
                    <div class="tns-selector-modal-title-container">
                        <h2 class="tns-selector-modal-title">Choose Network</h2>
                        <div class="js-tns-modal-close tns-selector-modal-close"></div>
                    </div>
                    <div class="js-tns-networks tns-selector-modal-networks-container">${ htmlNetworksTemplate }</div>
                </div>
            </div>`;

        $('body').append(htmlTemplate);
        bindNetworksModalComponents();

    };

    /**
     * Binds the networks selection modal's components
     */
    let bindNetworksModalComponents = () => {

        networksModal = $('.js-tns-modal');
        networksModalClose = $('.js-tns-modal-close');
        networksModalNetworkItem = $('.js-tns-network');

        networksModalClose.on('click', closeNetworksModal);
        networksModalNetworkItem.on('click', function () {

            $('.js-tns-network-check').removeClass('tns-selector-modal-network-check-active');
            $(this).find('.js-tns-network-check').addClass('tns-selector-modal-network-check-active');
            setCurrentNetwork($(this).data('network-tag'));
            closeNetworksModal();
            loadHTMLTemplate();
            loadNetworkData(getNetworkAPIURL());

        });

    };

    /**
     * Closes the networks selection modal
     */
    let closeNetworksModal = () => {

        networksModal.fadeOut(250, function() {
            $(this).addClass('hidden');
        });

    };

    /**
     * Shows the networks selection modal
     */
    let showNetworksModal = () => {

        networksModal.removeClass('hidden').fadeIn(250);

    };

    /**
     * Loads the topbar HTML template
     * @param {*} network
     */
    let loadHTMLTemplate = () => {

        const currentNetwork = getCurrentNetwork();
        let htmlTemplate = `
            <div class="topbar-network-stats topbar-network-stats-${ currentNetwork } js-tns-stats hidden">
                <a href="/" class="tns-logo-title-container tns-logo-title-container-${ currentNetwork }">
                    <div class="tns-logo-container tns-logo-container-${ currentNetwork }">
                        <div class="js-tns-logo tns-logo tns-logo-${ currentNetwork }"></div>
                    </div>
                    <div class="js-tns-name tns-title tns-title-${ currentNetwork }"></div>
                </a>
                <div class="tns-inner js-tns-inner tns-inner-${ currentNetwork }">
                    <div class="js-tns-inner-items tns-inner-items-container is-loading">
                        <!-- Network -->
                        <div class="tns-inner-item">
                            <div class="tns-inner-item-logo tns-inner-item-logo-${ currentNetwork } tns-inner-item-logo-network"></div>
                            <div class="tns-inner-item-stats">
                                <div class="tns-inner-item-stats-title tns-inner-item-stats-title-${ currentNetwork }">Consensus</div>
                                <div class="tns-inner-item-stats-data tns-inner-item-stats-data-${ currentNetwork } js-data-network"></div>
                            </div>
                        </div>
                        <!-- Block -->
                        <div class="tns-inner-item">
                            <div class="tns-inner-item-logo tns-inner-item-logo-${ currentNetwork } tns-inner-item-logo-block"></div>
                            <div class="tns-inner-item-stats">
                                <div class="tns-inner-item-stats-title tns-inner-item-stats-title-${ currentNetwork }">Block</div>
                                <div class="tns-inner-item-stats-data tns-inner-item-stats-data-${ currentNetwork } js-data-block"></div>
                            </div>
                        </div>
                        <!-- Last Block -->
                        <div class="tns-inner-item">
                            <div class="tns-inner-item-logo tns-inner-item-logo-${ currentNetwork } tns-inner-item-logo-last-block"></div>
                            <div class="tns-inner-item-stats">
                                <div class="tns-inner-item-stats-title tns-inner-item-stats-title-${ currentNetwork }">Last Block</div>
                                <div class="tns-inner-item-stats-data tns-inner-item-stats-data-${ currentNetwork } js-data-last-block"></div>
                            </div>
                        </div>
                        <!-- Average Block Time -->
                        <div class="tns-inner-item">
                            <div class="tns-inner-item-logo tns-inner-item-logo-${ currentNetwork } tns-inner-item-logo-average"></div>
                            <div class="tns-inner-item-stats">
                                <div class="tns-inner-item-stats-title tns-inner-item-stats-title-${ currentNetwork }">Avg. Block Time</div>
                                <div class="tns-inner-item-stats-data tns-inner-item-stats-data-${ currentNetwork } js-data-average"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Buttons -->
                    <div class="tns-buttons-container">
                        <a class="tns-button tns-button-primary tns-button-primary-${ currentNetwork } js-tns-explorer" target='_blank'>Explorer</a>
                        <a class="tns-button tns-button-transparent tns-button-transparent-${ currentNetwork } js-tns-change">Change Network</a>
                    </div>
                </div>
            </div>
        `;

        topbarContainer.html(htmlTemplate);
        bindTopbarComponents();
        hideLoading();

    };

    /**
     * Binds the topbar components for later manipulation
     */
    let bindTopbarComponents = () => {

        statsContainer = $('.js-tns-stats');
        currentNetworkTitleItem = $('.js-tns-name');
        currentNetworkNameItem = $('.js-data-network');
        blockItem = $('.js-data-block');
        lastBlockItem = $('.js-data-last-block');
        avgBlockTimeItem = $('.js-data-average');
        explorerButton = $('.js-tns-explorer');
        changeNetworkButton = $('.js-tns-change');
        tnsItems = $('.js-tns-inner-items');

        changeNetworkButton.on('click', showNetworksModal);

    };

    /**
     * Hide loading
     */
    let hideLoading = () => {

        statsContainer.removeClass('hidden');

    };

    /**
     * Show loading
     */
    let showLoading = () => {

        statsContainer.addClass('hidden');

    };

    /**
     * Hide data loading
     */
    let hideDataLoading = () => {

        tnsItems.removeClass('is-loading');

    };

    /**
     * Sets the currently active network
     */
    let setCurrentNetwork = (network = defaultNetwork) => {

        localStorage.setItem('currentNetwork', network);

    };

    /**
     * Gets the currently active network
     */
    let getCurrentNetwork = () => {

        /**
         * If we are on a specific network page, we change the topbar temporarily...
         */
        if(getNetworkPageTag()) {
            return getNetworkPageTag();
        }

        if (!localStorage.getItem('currentNetwork')) {
            setCurrentNetwork(defaultNetwork);
        }

        return localStorage.getItem('currentNetwork');

    };

    /**
     * Answers if we are in a particular network's page (Core, xDai, etc.)
     */
    let getNetworkPageTag = () => {

        let currentNetworkSection = '';

        Object.keys(networks).map((item, index) => {

            if ($('body').data('page-tag') === networks[item].tag) {

                currentNetworkSection = networks[item].tag;

            }

        });

        return currentNetworkSection;

    };

    /**
     * Gets the current network title
     */
    let getNetworkTitle = () => {

        return networks[getCurrentNetwork()].networkTitle;

    };

    /**
     * Gets the current network name
     */
    let getNetworkName = () => {

        return networks[getCurrentNetwork()].networkName;

    };

    /**
     * Gets the current network API URL
     */
    let getNetworkAPIURL = () => {

        return networks[getCurrentNetwork()].apiURL;

    };

    /**
     * Gets the current network Explorer URL
     */
    let getNetworkExplorerURL = () => {

        return networks[getCurrentNetwork()].explorerURL;

    };

    /**
     * Sets the current network Explorer URL
     */
    let setNetworkExplorerButtonURL = () => {

        explorerButton.attr({
            href: getNetworkExplorerURL()
        });

    };

    /**
     * Check if data loading interval is active
     */
    let isDataLoadingIntervalActive = () => {

        return networkDataInterval;

    }

    /**
     * Resets the data loading interval
     */
    let resetDataLoadingInterval = () => {

        clearInterval(networkDataInterval);
        networkDataInterval = undefined;

    }

    /**
     * Sets all the data
     */
    let loadNetworkData = (networkAPIURL) => {

        if (isDataLoadingIntervalActive()) {
            resetDataLoadingInterval();
        }

        setNetworkStaticData();
        loadNetworkDynamicData(networkAPIURL);

    };

    /**
     * Loads data from API server
     * @param {*} networkAPIURL
     */
    let loadNetworkDynamicData = (networkAPIURL) => {

        $.getJSON(networkAPIURL, (data) => {

            setNetworkDynamicData(data.Item.stat.M);

            if (!isDataLoadingIntervalActive()) {
                networkDataInterval = setInterval(() => {

                    loadNetworkDynamicData(networkAPIURL);

                }, apiDelay);
            }

        }).fail(function (e) {
            console.log(e);
        });

    }

    /**
     * Sets topbars' data that we already have
     */
    let setNetworkStaticData = () => {
        currentNetworkTitleItem.text(getNetworkTitle());
        currentNetworkNameItem.text(getNetworkName());
        setNetworkExplorerButtonURL();
    }

    /**
     * Sets topbars' data we got from the API
     */
    let setNetworkDynamicData = (stats) => {

        if (stats) {

            const lastBlockDate = new Date(Number.parseInt(stats.lastBlockTimestamp.N) * 1000);
            const lastBlockFormatted = ("0" + lastBlockDate.getHours()).slice(-2) + ":" +
                    ("0" + lastBlockDate.getMinutes()).slice(-2) + "<span class='tns-inner-item-stats-data-blink'>:</span>" +
                    ("0" + lastBlockDate.getSeconds()).slice(-2);

            blockItem.text('#' + stats.lastBlockNumber.N);
            lastBlockItem.html(lastBlockFormatted);
            avgBlockTimeItem.text(Number.parseFloat(stats.avgBlockTime.N).toFixed(2) + ' s');

            hideDataLoading();

        }
        else {
            console.error("Couldn't set network data. Undefined stats.");
        }

    };

    /**
     * Init
     */
    let initialize = () => {

        loadHTMLTemplate();
        loadNetworkData(getNetworkAPIURL());
        loadNetworksModal();

    };

    initialize();

});
