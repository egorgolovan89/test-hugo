/**
 * DApps' tabs mobile version dragging
 */
$(function () {
    'use strict'

    let walletsList = $('.js-wallets-list');

    if (!walletsList.length) {

        return;

    }

    const walletsLoading = $('.js-wallets-loading');
    const baseImagesPath = 'assets/img/wallets/wallet-logos/'
    const walletsListJSON = [
        {
            backgroundColor: '#fff',
            image: 'wallets-alphawallets',
            moreInfoURL: 'https://alphawallet.com',
            title: 'AlphaWallet',
            width: '26px',
            height: '17px'
        },
        {
            backgroundColor: '#341790',
            image: 'wallets-citowise',
            moreInfoURL: 'https://citowise.com/',
            title: 'Citowise',
            width: '24px',
            height: '22px'
        },
        {
            backgroundColor: '#ffffff',
            image: 'wallets-coinomi',
            moreInfoURL: 'https://www.coinomi.com/en/',
            title: 'Coinomi',
            width: '24px',
            height: '24px'
        },
        {
            backgroundColor: '#373947',
            image: 'wallets-ledger',
            moreInfoURL: 'https://forum.poa.network/t/wallet-ledger-nano-s-hardware/1816/2',
            title: 'Ledger',
            width: '24px',
            height: '24px'
        },
        {
            backgroundColor: '#ffca86',
            image: 'wallets-metamask',
            moreInfoURL: 'https://forum.poa.network/t/wallet-metamask-extension/1819/2',
            title: 'Metamask',
            width: '22px',
            height: '24px'
        },
        {
            backgroundColor: '#5093b8',
            image: 'wallets-mycrypto',
            moreInfoURL: 'https://forum.poa.network/t/wallet-mycrypto/1904',
            title: 'MyCrypto',
            width: '24px',
            height: '24px'
        },
        {
            backgroundColor: '#68baaa',
            image: 'wallets-myetherwallet',
            moreInfoURL: 'https://forum.poa.network/t/wallet-myetherwallet-mew-web-mobile-web/1817/2',
            title: 'MyEtherWallet',
            width: '24px',
            height: '27px'
        },

        {
            backgroundColor: '#8c45ff',
            image: 'wallets-niftywallet',
            moreInfoURL: 'https://forum.poa.network/t/nifty-wallet-guide/1789',
            title: 'Nifty Wallet',
            width: '32px',
            height: '30px'
        },
        {
            backgroundColor: '#fff',
            image: 'wallets-ownbit',
            moreInfoURL: 'https://ownbit.io/en/',
            title: 'Ownbit',
            width: '25px',
            height: '26px'
        },
        {
            backgroundColor: '#fff',
            image: 'wallets-portis',
            moreInfoURL: 'https://portis.io',
            title: 'Portis',
            width: '18px',
            height: '28px'
        },
        {
            backgroundColor: '#1ea2ff',
            image: 'wallets-trust-wallet',
            moreInfoURL: 'https://forum.poa.network/t/wallet-trust-wallet/1818/',
            title: 'Trust Wallet',
            width: '20px',
            height: '24px'
        },

        // {
        //     backgroundColor: '#000000',
        //     image: 'wallets-trezor',
        //     moreInfoURL: '#',
        //     title: 'Trezor',
        //     width: '16px',
        //     height: '24px'
        // }
    ];

    /**
     * Loads the wallets
     */
    let loadWalletsListItems = () => {

        let walletItems = walletsListJSON.map((item, index) => {
            let bgImage = baseImagesPath + item.image + '@3x.png';

            return `
                <a class="wallets-list-item" href="${item.moreInfoURL}" target="_blank">
                    <span
                        class="wallets-list-item-image"
                        style="background-color: ${item.backgroundColor}; background-size: ${item.width} ${item.height}; background-image: url('${bgImage}');"
                    ></span>
                    <span class="wallets-list-item-content">
                        <span class="wallets-list-item-title">${item.title}</span>
                        <span class="wallets-list-item-more-info">More Info</a>
                    </span>
                </a>
            `;
        });

        walletsLoading.remove();
        walletsList.removeClass('hidden').html(walletItems);

    };

    /**
     * Init
     */
    let initialize = () => {

        loadWalletsListItems();

    };

    initialize();

});
