/**
 * Progressbar on gamepad for 'networks' page
 */
$(function () {
	'use strict';
	let currentpbvalue = +$(".poa-games-progressbar-line").attr("data-value"),
		maxpbvalue = +$(".poa-games-progressbar-line").attr("data-max"),
		pbpercent = currentpbvalue * 100 / maxpbvalue;

	$(".poa-games-progressbar-line").css("width", pbpercent + "%");

});

