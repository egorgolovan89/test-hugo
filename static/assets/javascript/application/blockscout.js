$(function () {
	// Tokens
	if ($("body").hasClass("blockscout-page")) {
		let transactionsAnimated = 0;
		$(window).scroll(function() {
			if (transactionsAnimated === 0) {
				let hT = $(".real-time").offset().top,
					hH = $(".real-time").outerHeight(),
					wH = $(window).height(),
					wS = $(this).scrollTop();
				if (wS > (hT+hH-wH)){
					$(".real-time-transactions img").addClass("slideInLeft");
					transactionsAnimated = 1;
				}
			}
		});


		let tokensloaded = 0;
		$(window).scroll(function() {
			if (tokensloaded === 0) {
				let hT = $(".blockscout-token-support").offset().top,
					hH = $(".blockscout-token-support").outerHeight(),
					wH = $(window).height(),
					wS = $(this).scrollTop();
				if (wS > (hT+hH-wH)){
					function showTransactions () {
						$(".blockscout-token-examples").slideToggle(200);
						$(".token-loader").slideToggle(200);
					}
					setTimeout(showTransactions, 1500);
					tokensloaded = 1;
				}
			}
		});
	}

	// Parralax
	function setParralax () {
		let parralaxObj = $(".blockscout-api-parralax");
		parralaxObj.animate({scrollTop: parralaxObj[0].scrollHeight}, 30000);
		parralaxObj.animate({scrollTop: -(parralaxObj[0].scrollHeight)}, 30000);
	}

	if ($("body").hasClass("blockscout-page")) {
		setParralax();
		setInterval(setParralax, 100);
	}


	// Carousel
	$(".blockscout-carousel").slick({
		adaptiveHeight: true,
		autoplay:true,
		autoplaySpeed:3000,
		arrows:false,
	});

	if ($("body").hasClass("blockscout-page")) {
		$('.blockscout-carousel').on('afterChange', function() {
		    var dataId = $('.slick-current').attr("data-slick-index");    
		    $("li[data-slide]").removeClass("active");
		    $("li[data-slide]").eq(dataId).addClass("active");
		});
	}

	$("li[data-slide]").click(function() {
		$(".blockscout-carousel").slick('slickPause');
		let slideno = $(this).data('slide');
		$('.blockscout-carousel').slick('slickGoTo', slideno - 1);
		$("li[data-slide]").removeClass("active");
		$(this).addClass("active");
	});

});