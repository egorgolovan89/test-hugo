/**
 * Load main events
 */

jQuery.fn.extend({
    loadMainEvents: function(data) {

        let mainEventsContainer = $('.js-network-events-section-events-list');

        if (!mainEventsContainer) return;

        const maxNewsList = 5;
        let htmlTemplate = '';
        let isActive = '';
        let eventUpocomingStatus = '';

        for (let cont = 0; cont < maxNewsList; cont++) {

            // First one is active
            (cont === 3) ? isActive = 'active' : isActive = '';
            // Check if upcoming or past event
            ($.fn.isUpcomingEvent(data[cont].dateStart)) ? eventUpocomingStatus = Object.keys($.fn.eventProximity())[0] : eventUpocomingStatus = Object.keys($.fn.eventProximity())[1];

            htmlTemplate += `
                <div
                    class="${ isActive } network-events-section-events-list-item js-network-events-section-events-list-item"
                    data-meetup-address="${ data[cont].address }"
                    data-meetup-description="${ data[cont].description }"
                    data-meetup-img="${ data[cont].image }"
                    data-meetup-title="${ data[cont].title }"
                    data-meetup-url="${ data[cont].url }"
                >
                    <div class="the-date text-center">
                        <span class="the-date-number">${ $.fn.getEventDay(data[cont].dateStart) }</span>
                        <span class="the-date-month">${ $.fn.getEventMonthString(data[cont].dateStart) }</span>
                    </div>
                    <div class="the-info">
                        <h4 class="the-title">${ data[cont].title }</h4>
                        <p class="the-proximity ${ eventUpocomingStatus }">${ $.fn.eventProximity()[eventUpocomingStatus] }</p>
                    </div>
                </div>`;
        }

        mainEventsContainer.html('');
        mainEventsContainer.append(htmlTemplate);
        this.removeMainEventsLoading();
    },
    removeMainEventsLoading: function() {

        $('.js-network-events-section-loading').addClass('hidden');
        $('.js-network-events-container').removeClass('hidden');

    }
});
