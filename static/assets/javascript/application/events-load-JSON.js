/**
 * Events list
 */
$(function () {
    'use strict'

    /**
     * Initialize
     */
    let initialize = function (data) {

        let tidyEventsArray = $.fn.sortEventsByDate($.fn.cleanEventsJSON(data));

        $.fn.loadMainEvents(tidyEventsArray);
        $.fn.loadEventsTabs(tidyEventsArray);
        $.fn.mainEventsListClickEvents();
        $.fn.eventsTabsFilterInit();

    };

    /**
     * Get data from JSON file
     */
    let loadEventsData = async () => {

        try {

            const response = await fetch('assets/JSON/events.json');

            initialize(await response.json());

        } catch (err) {

            console.error('Error fetching events JSON file: ' + err)

        }

    };

    loadEventsData();

});
