/**
 * Loads site menu's links
 */
$(function () {
    'use strict'

    const siteMenuContainer = $('.js-navbar-nav');
    const siteNetworksContainer = $('.js-header-network-links');

    if (!siteMenuContainer.length || !siteNetworksContainer.length) {

        return;

    }

    const networkLinks = [
      {
          href: "https://poa.network/",
          target: null,
          text: "POA Network",
          logo: "assets/img/network-logos/poa.png"
      },
      {
          href: "https://xdaichain.com/xdai",
          target: null,
          text: "xDai Stable Chain",
          logo: "assets/img/network-logos/xdai.png"
      },
      {
          href: "https://poa.network/blockscout",
          target: null,
          text: "BlockScout",
          logo: "assets/img/network-logos/blockscout.png"
      },
      {
          href: "https://poa.network/bridge",
          target: null,
          text: "TokenBridge",
          logo: "assets/img/network-logos/tokenbridge.png"
      }
    ];

    const menuLinks = [
        {
            href: "events",
            target: null,
            text: "Events"
        },
        {
            href: "https://forum.poa.network/",
            target: "_blank",
            text: "Forum"
        }
    ];

    /**
     * Loads the menu's items
     */
    let loadMenuItems = function() {

        const menuHTML = menuLinks.map((item, index) => {
            const target = item.target ? `target="${ item.target }"` : '';

            return `<li class="nav-item"><a href="${ item.href }" ${ target }>${ item.text }</a></li>`
        });

        siteMenuContainer.append(menuHTML);

    };

    let loadNetworkItems = function() {

        const networksHTML = networkLinks.map((item, index) => {
            const target = item.target ? `target="${ item.target }"` : '';

            return `<a class="header-int-links-item" href="${ item.href }" ${ target }>
                <img src="${ item.logo }">
                <span>${ item.text }</span>
            </a>`
        });

        siteNetworksContainer.append(networksHTML);

    };

    let setActiveNetwork = function() {
        let currentNetwork = '';

        const networkHrefs = networkLinks.map((item, index) => {
            const href = item.href;
            if ($("body").attr("data-active-nav-link") === href) {
                currentNetwork = href;
            }

            $(".main-nav a").removeClass("active");
            $(".main-nav a[href='" + currentNetwork + "']").addClass("active");
        });

    };



    /**
     * Initialize stuff
     */
    let initialize = function() {

        loadMenuItems();
        loadNetworkItems();
        setActiveNetwork();

    };

    initialize();

});
