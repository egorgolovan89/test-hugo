/**
 * Reads the list of contributors from contributors.js, and displays them
 */
$(function () {
    'use strict'

    let contributorsImagesContainer = $('#nci');

    if (!contributorsImagesContainer.length) {

        return;

    }

    if (!contributorsJSON) {

        console.error('Could not find the contributors.js file.');

        return;

    }

    const sliderDisplacement = 1;   // pixels to move each step
    const sliderSpeed = 25;          // in ms (less == faster)

    let rows = [[], [], []];
    let contributorsImagesRowDivStart = '<div class="network-contributors-images-row">';
    let contributorsImagesAppend = '';
    let sliderCurrentDisplacement = 0;
    let contributorsSlideInterval = undefined;

    /**
     * Get contributors, put them into rows
     */
    let fillContributorsRows = function () {

        const TOP_ROW = 0;
        const MIDDLE_ROW = 1;
        const BOTTOM_ROW = 2;

        const topRowLength = 20;
        const middleRowLength = 19;
        const bottomRowLength = 20;

        // Items' amount per row (they are few for now, automatize if this gets worst).
        rows[TOP_ROW] = contributorsJSON.contributors.splice(0, topRowLength);
        rows[MIDDLE_ROW] = contributorsJSON.contributors.splice(0, middleRowLength);
        rows[BOTTOM_ROW] = contributorsJSON.contributors.splice(0, bottomRowLength);

        for (let i = 0; i < rows.length; i++) {

            contributorsImagesAppend = contributorsImagesAppend + contributorsImagesRowDivStart;

            for (let ii = 0; ii < rows[i].length; ii++) {

                contributorsImagesAppend = contributorsImagesAppend + `
                    <a
                        class="network-contributors-image"
                        href="https://github.com/` + rows[i][ii].name.replace('@', '') + `"
                        style="background-image: url('` + rows[i][ii].imageUrl + `');"
                        target="_blank"
                        title="` + rows[i][ii].name + `"
                    ></a>
                `;

            }

            contributorsImagesAppend = contributorsImagesAppend + '</div>';

        }

        contributorsImagesAppend = '<div class="network-contributors-images-slide">' + contributorsImagesAppend + '</div>'

        contributorsImagesContainer.append(contributorsImagesAppend);
        contributorsImagesContainer.append(contributorsImagesAppend);

    };

    /**
     * Move slide
     */
    let moveContributorsSlide = function () {

        let slideWidth = parseInt($(".network-contributors-images-slide").css("width"));

        contributorsSlideInterval = setInterval(function () {

            if (sliderCurrentDisplacement == -slideWidth) {

                sliderCurrentDisplacement = 0;
                return;

            }

            sliderCurrentDisplacement = sliderCurrentDisplacement - sliderDisplacement;
            contributorsImagesContainer.css({
                'transform': 'translateX(' + sliderCurrentDisplacement + 'px)'
            });

        }, sliderSpeed);

    };

    let setSliderMouseEvents = function() {

        $(".network-contributors-images-outer").on("mouseenter", function() {
            clearInterval(contributorsSlideInterval);
        });

        $(".network-contributors-images-outer").on("mouseleave", function() {
            moveContributorsSlide();
        });

    };

    function startContribSlider () {
        fillContributorsRows();
        moveContributorsSlide();
        setSliderMouseEvents();
    }

    setTimeout(startContribSlider, 2000);

    

});
