/**
 * Filter events by category
 */
jQuery.fn.extend({
    eventsTabsFilterInit: function () {
        'use strict'

        let eventsTab = $('.js-events-tab');

        if (!eventsTab.length) {

            return;

        }

        let eventsItemsAll = $('.js-filtered-event-item');

        /**
         * Check if category exists
         */
        const categoryExists = function(category) {

            return $('[data-event-category=' + category + ']').length;

        }

        /**
         * Activate tab
         * @param {*} category
         */
        const activateTab = function(category) {

            eventsTab.removeClass('active');
            $('[data-event-category-filter=' + category + ']').addClass('active');

        }

        /**
         * Show Events by category
         */
        const showEventsByCategory = function(category) {

            let eventsItems = (category == 'all') ? eventsItemsAll : $('[data-event-category=' + category + ']');

            eventsItemsAll = $('.js-filtered-event-item');

            $('.js-filtered-events-list').fadeOut(150, function() {

                eventsItemsAll.removeClass('visible').addClass('hidden');
                eventsItems.removeClass('hidden').addClass('visible');
                eventsItems.last().addClass('no-border');

                $('.js-filtered-events-list').fadeIn(150);

            });

        }

        /**
         * Filter by selected category
         * @param {*} category
         */
        const filterEvents = function(category) {

            const activeCategory = (!category || !categoryExists(category)) ? 'all' : category;

            showEventsByCategory(activeCategory);
            activateTab(activeCategory);

        }

        /**
         * Set active category
         */
        eventsTab.on("click", function() {

            if ($(this).hasClass('active')) return;

            const category = $(this).data('event-category-filter') ? $(this).data('event-category-filter') : 'all';

            filterEvents(category);

        });

    }
});