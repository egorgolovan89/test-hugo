
if ($("body").hasClass("x-dai-page")) {

	var ctx = $("#doughnut-chart");

	var data = {
	    datasets: [{
	        backgroundColor: ["#E1EEFE", "#3B94E6","#47C7A9","#EBA329","#8221DB","#D42CA3"],
	        hoverBackgroundColor: ["#E1EEFE", "#3B94E6","#47C7A9","#EBA329","#8221DB","#D42CA3"],
	        borderWidth: 0,
	        data: [73,12.5,1,8.5,4,1]
	    }]
	};

	var options = {
		tooltips: {
		    enabled: false
		  },
		  segmentShowStroke: {
		  	enabled: false
		  }
	}



	var myDoughnutChart = new Chart(ctx, {
	    type: 'doughnut',
	    data: data,
	    options: options
	});

}