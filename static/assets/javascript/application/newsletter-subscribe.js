/**
 * Newsletter subscription
 */
$(function () {

  var body = $("body");
  var input = $(".js-subscribe-input");
  var subscribeButton = $(".js-subscribe-btn");

  var loaderHtml = ' \
        <div class="loader-container">  \
          <div class="loader"> \
            <div class="loader-i"></div> \
            <div class="loader-i"></div> \
            <div class="loader-i"></div> \
            <div class="loader-i"></div> \
            <div class="loader-i"></div> \
            <div class="loader-i"></div> \
          </div> \
        </div>';

  var newsletterSubscribe = function () {

    if (!input.val()) {
      return;
    }

    body.append(loaderHtml);

    var inputData = {

      "subscriber": input.val(),
      "mailingListId": "f29RQi3Lzt"

    };

    $.ajax({
      url: "https://notarycoin-subscription.herokuapp.com/subscribe",
      type: "POST",
      data: JSON.stringify(inputData),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    })
      .always(function () {

        $(".loader-container").remove();

      })
      .done(function (data) {

        if (data.success) {

          swal("Success", "Subscription created", "success");
          input.val('');

        } else if (data.warning) {

          swal("Warning", data.warning.message, "warning");

        } else {

          swal("Error", data.error.message, "error");

        }
      })
      .fail(function (err) {

        swal("Error", data.error.message, "error");

      });

  }

  subscribeButton.on("click", function () {

    newsletterSubscribe();

  });

  input.keypress(function (event) {

    var key = event.which;

    if (key == 13) {

      event.preventDefault();
      newsletterSubscribe();

    }

  });

});
