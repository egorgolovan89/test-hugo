/**
 * Events list
 */
jQuery.fn.extend({
    mainEventsListClickEvents: function () {
        'use strict'

        /**
         * Gets the active item information
         */
        let getActiveEventItemData = function () {

            let activeEventItem = $('.js-network-events-section-events-list-item.active');

            if (!activeEventItem) {

                console.error("No active event.");
                return false;

            }

            return {
                url: activeEventItem.data('meetup-url'),
                image: activeEventItem.data('meetup-img'),
                address: activeEventItem.data('meetup-address'),
                description: activeEventItem.data('meetup-description'),
                title: activeEventItem.data('meetup-title')
            }

        };

        /**
         * Displays the active item information in the main event content block
         */
        let displayActiveEvent = function () {

            let eventDetails = $('.js-network-events-section-event-display');
            let htmlTemplate = '';
            let activeItem = getActiveEventItemData();

            if (!activeItem || !eventDetails) return;

            eventDetails.fadeOut(100, function () {

                htmlTemplate = `
                    <a
                        href="${ activeItem.url }"
                        target="_blank"
                    >
                        <span class="the-image-container">
                            <span
                                class="the-image content-ratio"
                                style="background-image: url('${ activeItem.image }');"
                            ></span>
                        </span>
                        <span class="the-title">${ activeItem.title }</span>
                        ${ activeItem.address && (activeItem.address != 'undefined') ? '<span class="the-address">' + activeItem.address + '</span>' : '' }
                        <span class="the-description">${ activeItem.description }</span>
                        <span class="view-more" >Find out more</span>
                    </a>
                `;

                eventDetails.html('');
                eventDetails.append(htmlTemplate);
                eventDetails.fadeIn(100);

            });

        };

        /**
         * Set element's events
         */
        let setMainEventsListItemsEvents = function () {

            $('.js-network-events-section-events-list-item').on('click', function () {

                if ($(this).hasClass('active')) return;

                $('.js-network-events-section-events-list-item').removeClass('active');
                $(this).addClass('active');
                displayActiveEvent();

            });

        };

        /**
         * Initialize
         */
        let initialize = function () {

            displayActiveEvent();
            setMainEventsListItemsEvents();

        };

        initialize();

    }
});
