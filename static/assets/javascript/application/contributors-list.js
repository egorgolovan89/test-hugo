var contributorsJSON = {
    "contributors": []
};

var parselinks = [
    "https://api.github.com/repos/poanetwork/blockscout/contributors?per_page=100",
    "https://api.github.com/repos/poanetwork/tokenbridge/contributors?per_page=100",
    "https://api.github.com/repos/poanetwork/hbbft/contributors?per_page=100"
];

if ($(".network-contributors-section").length) {
   for (var i = 0; i < parselinks.length + 1; i++) {
       $.getJSON(
           parselinks[i],
           function(data) {
               $.each( data, function( i, user ) {
                   var pushedUser = {
                       "imageUrl" : user.avatar_url,
                       "name" : user.login
                   }
                   contributorsJSON.contributors.push(pushedUser);
               });
           }
       );​
   }

   const filteredArr = contributorsJSON.contributors.reduce((acc, current) => {
     const x = acc.find(item => item.user.login === current.user.login);
     if (!x) {
       return acc.concat([current]);
       contributorsJSON.contributors = acc;
     } else {
       return contributorsJSON.contributors;
     }
   }, []);
}

// contributorsJSON = {
//     "contributors": [
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/15729797?s=96&v=4",
//             "name": "@5chdn"
//         },
//         {
//             "imageUrl": "https://avatars3.githubusercontent.com/u/17620007?s=96&v=4",
//             "name": "@acravenho"
//         },
//         {
//             "imageUrl": "https://avatars0.githubusercontent.com/u/7894725?s=96&v=4",
//             "name": "@afck"
//         },
//         {
//             "imageUrl": "https://avatars0.githubusercontent.com/u/20793260?s=96&v=4",
//             "name": "@akolotov"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/911605?s=96&v=4",
//             "name": "@alexgaribay"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/840531?s=96&v=4",
//             "name": "@amandasposito"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/14284654?s=96&v=4",
//             "name": "@andogro"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/23522179?s=96&v=4",
//             "name": "@ArseniiPetrovich"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/1641169?s=96&v=4",
//             "name": "@arthurra"
//         },
//         {
//             "imageUrl": "https://avatars3.githubusercontent.com/u/6567687?s=96&v=4",
//             "name": "@ayrat555"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/955793?s=96&v=4",
//             "name": "@bitwalker"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/2916797?s=96&v=4",
//             "name": "@c0gent"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/1279673?s=96&v=4",
//             "name": "@cernivec"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/576796?s=96&v=4",
//             "name": "@chrismccord"
//         },
//         {
//             "imageUrl": "https://avatars3.githubusercontent.com/u/14067096?s=96&v=4",
//             "name": "@dennis00010011b"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/9577225?s=96&v=4",
//             "name": "@DrPeterVanNostrand"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/11424434?s=96&v=4",
//             "name": "@Dzol"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/1415489?s=96&v=4",
//             "name": "@EvgenKor"
//         },
//         {
//             "imageUrl": "https://avatars0.githubusercontent.com/u/27698968?s=96&v=4",
//             "name": "@feliperenan"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/7842646?s=96&v=4",
//             "name": "@ferigis"
//         },
//         {
//             "imageUrl": "https://avatars3.githubusercontent.com/u/3315606?s=96&v=4",
//             "name": "@fernandomg"
//         },
//         {
//             "imageUrl": "https://avatars0.githubusercontent.com/u/417134?s=96&v=4",
//             "name": "@fvictorio"
//         },
//         {
//             "imageUrl": "https://avatars0.githubusercontent.com/u/3245976?s=96&v=4",
//             "name": "@germsvel"
//         },
//         {
//             "imageUrl": "https://avatars0.githubusercontent.com/u/1440642?s=96&v=4",
//             "name": "@gfreh"
//         },
//         {
//             "imageUrl": "https://avatars0.githubusercontent.com/u/424628?s=96&v=4",
//             "name": "@igorbarinov"
//         },
//         {
//             "imageUrl": "https://avatars3.githubusercontent.com/u/2921671?s=96&v=4",
//             "name": "@igorffs"
//         },
//         {
//             "imageUrl": "https://avatars0.githubusercontent.com/u/781752?s=96&v=4",
//             "name": "@jimmay5469"
//         },
//         {
//             "imageUrl": "https://avatars0.githubusercontent.com/u/20226535?s=96&v=4",
//             "name": "@katibest"
//         },
//         {
//             "imageUrl": "https://avatars3.githubusercontent.com/u/19871487?s=96&v=4",
//             "name": "@KORuL"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/298259?s=96&v=4",
//             "name": "@KronicDeth"
//         },
//         {
//             "imageUrl": "https://avatars3.githubusercontent.com/u/10884247?s=96&v=4",
//             "name": "@Lucasnar"
//         },
//         {
//             "imageUrl": "https://avatars3.githubusercontent.com/u/1144028?s=96&v=4",
//             "name": "@mariano-aguero"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/229473?s=96&v=4",
//             "name": "@masonforest"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/303122?s=96&v=4",
//             "name": "@matiasgaratortiz"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/759721?s=96&v=4",
//             "name": "@MattMSumner"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/110577?s=96&v=4",
//             "name": "@mbr"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/8477052?s=96&v=4",
//             "name": "@natlg"
//         },
//         {
//             "imageUrl": "https://avatars0.githubusercontent.com/u/1939415?s=96&v=4",
//             "name": "@pablofullana"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/16716926?s=96&v=4",
//             "name": "@pashagonchar"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/4614574?s=96&v=4",
//             "name": "@patitonar"
//         },
//         {
//             "imageUrl": "https://avatars3.githubusercontent.com/u/2278876?s=96&v=4",
//             "name": "@phahulin"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/9360827?s=96&v=4",
//             "name": "@rstormsf"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/1958868?s=96&v=4",
//             "name": "@sabondano"
//         },
//         {
//             "imageUrl": "https://avatars3.githubusercontent.com/u/4807398?s=96&v=4",
//             "name": "@SiddigZeidan"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/404834?s=96&v=4",
//             "name": "@sistemico"
//         },
//         {
//             "imageUrl": "https://avatars0.githubusercontent.com/u/3826578?s=96&v=4",
//             "name": "@Stamates"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/36401896?s=96&v=4",
//             "name": "@szlavikr"
//         },
//         {
//             "imageUrl": "https://avatars0.githubusercontent.com/u/3968168?s=96&v=4",
//             "name": "@tatianaoudine"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/966985?s=96&v=4",
//             "name": "@tmecklem"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/4421917?s=96&v=4",
//             "name": "@unjapones"
//         },
//         {
//             "imageUrl": "https://avatars0.githubusercontent.com/u/33550681?s=96&v=4",
//             "name": "@varasev"
//         },
//         {
//             "imageUrl": "https://avatars1.githubusercontent.com/u/4341812?s=96&v=4",
//             "name": "@vbaranov"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/5529505?s=96&v=4",
//             "name": "@vkomenda"
//         },
//         {
//             "imageUrl": "https://avatars3.githubusercontent.com/u/41199128?s=96&v=4",
//             "name": "@VladimirNovgorodov"
//         },
//         {
//             "imageUrl": "https://avatars3.githubusercontent.com/u/988849?s=96&v=4",
//             "name": "@vyorkin"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/7325903?s=96&v=4",
//             "name": "@Ykisialiou"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/452?s=96&v=4",
//             "name": "@yrashk"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/4015436?s=96&v=4",
//             "name": "@gabitoesmiapodo"
//         },
//         {
//             "imageUrl": "https://avatars2.githubusercontent.com/u/201871?s=96&v=4",
//             "name": "@mgarciap"
//         }
//     ]
// };