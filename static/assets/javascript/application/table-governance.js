/**
 * Governance table (mobile only)
 */
$(function () {

    "use strict";

    if (!$(".table-governance").length) {

        return;

    }

    $(".table-governance ul").on("click", "li", function () {

        let pos = $(this).index() + 2;

        $("tr").find('td:not(:eq(0))').hide();
        $('td:nth-child(' + pos + ')').css('display', 'table-cell');
        $("tr").find('th:not(:eq(0))').hide();
        $('li').removeClass('active');
        $(this).addClass('active');

    });

});