/**
 * Bridge Play Video
 */
$(function () {

    "use strict"; // Start of use strict

    let videoPlay = $('#play-video');

    if (!videoPlay.length) {

        return;

    }

    videoPlay.on('click', function (ev) {
        $('.iframe-img-block').addClass('d-none');
        $("#video")[0].src += "&autoplay=1";
        ev.preventDefault();

      });

});