/**
 * question link items
 */
$(function () {

    "use strict";

    if (!$('.question-link-items').length) {

        return;

    }

    $('.question-link-items h4').on('click', function () {

      $('.question-link-items h4').removeClass('active');
      $(this).addClass('active');

    });

});