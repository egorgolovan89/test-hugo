/**
 * Mobile menu
 */
$(function () {
    "use strict";

    let menuWrapper  = $('.menu-wrapper');

    if (!menuWrapper.length) {

        return;

    }

    menuWrapper.on('click', function () {
      $('.hamburger-menu').toggleClass('animate');
      $('.hidden-nav').toggleClass('active');
      $('.hidden-btn').toggleClass('active');
    });

});