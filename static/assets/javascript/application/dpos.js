/**
 * Dpos page js
 */
$(function () {

	function changeSlide () {
		var slide = $(".pic-slider-slide"),
			firstSlide = $(".pic-slider-slide-first"),
			secondSlide = $(".pic-slider-slide-second"),
			thirdSlide = $(".pic-slider-slide-third");
			secondSlide.removeClass("pic-slider-slide-second").addClass("pic-slider-slide-first");
			thirdSlide.removeClass("pic-slider-slide-third").addClass("pic-slider-slide-second");
			firstSlide.removeClass("pic-slider-slide-first").addClass("pic-slider-slide-third");

			var activeSlideNum = $(".pic-slider-slide.pic-slider-slide-first").index();
			$(".explorer-controlls li").removeClass("active").eq(activeSlideNum).addClass("active");
	}


	var sliderAnim = setInterval(changeSlide, 4000);

	

	$(".explorer-controlls li").click(function () {
		$(".explorer-controlls li").not(this).removeClass("active");
		$(this).addClass("active");
		clearInterval(sliderAnim);
		var pos = $(this).index();
		if (pos == 0) {
			$(".pic-slider-slide").eq(0).removeClass("pic-slider-slide-second pic-slider-slide-third").addClass("pic-slider-slide-first");
			$(".pic-slider-slide").eq(1).removeClass("pic-slider-slide-first pic-slider-slide-third").addClass("pic-slider-slide-second");
			$(".pic-slider-slide").eq(2).removeClass("pic-slider-slide-first pic-slider-slide-second").addClass("pic-slider-slide-third");
		} else if (pos == 1) {
			$(".pic-slider-slide").eq(0).removeClass("pic-slider-slide-first pic-slider-slide-second").addClass("pic-slider-slide-third");
			$(".pic-slider-slide").eq(1).removeClass("pic-slider-slide-second pic-slider-slide-third").addClass("pic-slider-slide-first");
			$(".pic-slider-slide").eq(2).removeClass("pic-slider-slide-first pic-slider-slide-third").addClass("pic-slider-slide-second");
		} else {
			$(".pic-slider-slide").eq(0).removeClass("pic-slider-slide-third pic-slider-slide-first").addClass("pic-slider-slide-second");
			$(".pic-slider-slide").eq(1).removeClass("pic-slider-slide-first pic-slider-slide-second").addClass("pic-slider-slide-third");
			$(".pic-slider-slide").eq(2).removeClass("pic-slider-slide-second pic-slider-slide-third").addClass("pic-slider-slide-first");
		}
	});


	$(".carousel-body").slick({
		adaptiveHeight: true,
		// autoplay: true,
		// autoplaySpeed: 3000,
		prevArrow: "<button class='slick-arrow slick-prev'>",
		nextArrow: "<button class='slick-arrow slick-next'>"
	});

});