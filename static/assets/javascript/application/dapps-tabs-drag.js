/**
 * DApps' tabs mobile version dragging
 */
$(function () {
    'use strict'

    let dappsNavDrag = $('#dappsNavDrag');

    if (!dappsNavDrag.length) {

        return;

    }

    let isDragging = false;
    let dappsNavContainerWidth = 0;
    let dappsNavDragWidth = 0;
    let allowedHorizontalDragDisplacement = 0;
    let lastX = 0;

    /**
     * Initialize
     */
    const initialize = function() {

        dappsNavContainerWidth = $('.JSDappsNavContainer').width();
        dappsNavDragWidth = dappsNavDrag.width();

        if (!isDraggable()) return;

        dappsNavDrag.css({
            'margin-left': '0'
        });
        allowedHorizontalDragDisplacement = -Math.trunc(dappsNavDragWidth - dappsNavContainerWidth);

    }

    /**
     * Is draggable
     */
    const isDraggable = function() {

        return dappsNavDragWidth > dappsNavContainerWidth;

    }

    /**
     * Get mouse X coordinate
     * @param {*} event
     */
    const getMouseX = function(event) {

        if (event.pageX) {

            return parseInt(event.pageX);

        }

        return parseInt(event.originalEvent.touches[0].pageX);

    }

    /**
     * Get drag displacement
     */
    const getDragDisplacement = function(dragMargin, currentMouseX, _lastX) {

        // First let's check some bounds
        // Is moving right
        if (currentMouseX > _lastX) {

            if (dragMargin >= 0) {

                return 0;

            }

        }
        // Is moving left
        else if (currentMouseX < _lastX) {

            if (dragMargin <= allowedHorizontalDragDisplacement) {

                return allowedHorizontalDragDisplacement;

            }

        }

        return (dragMargin + (currentMouseX - _lastX))

    }

    /**
     * Start tabs dragging
     */
    dappsNavDrag.on('mousedown touchstart', function(event) {

        if (!isDraggable()) return;

        isDragging = true;

        lastX = getMouseX(event);

    });

    /**
     * Is dragging
     */
    dappsNavDrag.on('mousemove touchmove', function(event) {

        if (!isDragging) return;

        const currentMouseX = getMouseX(event);

        let dragDisplacement = getDragDisplacement(parseInt(dappsNavDrag.css('margin-left')), currentMouseX, lastX);

        dappsNavDrag.css({
            'margin-left': dragDisplacement + 'px'
        });

        lastX = currentMouseX;

    });

    /**
     * Stop tabs dragging
     */
    dappsNavDrag.on('mouseup touchend', function(event) {

        isDragging = false;

        lastX = 0

    });

    /**
     * Reset on window resizing
     */
    $(window).on('resize', function() {
        initialize();
    });

    initialize();

});
