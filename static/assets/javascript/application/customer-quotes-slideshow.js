$(function () {

    "use strict";

    if (!$('.js-customer-quotes-slideshow').length) {
        return;
    }

    const slideData = [
        {
            company: 'Swarm City Architect',
            logoHeight: '58px',
            logoImage: 'swarm-city@3x.png',
            logoWidth: '160px',
            name: 'kingflurkel',
            quote: 'At Swarm City we see it as our mission to bring amazing decentralized tech like Ethereum to the masses. Currently, Ethereum is still in its early days, yet we don’t want our communities to wait on usable tech. POA created a solution to bridge Ethereum networks, so we can use them to do transactions on, and then resolve them on the mainnet. This keeps transaction costs down.<br /><br />Swarm City is a community of early adopters in crypto, and we are not afraid to test out new concepts. That’s why POA and Swarm City are a natural match to progress the state of logos together. Swarm City is proud to be using POA’s bridging solution to make our economy work as soon as possible.'
        },
        {
            company: 'Founder & CEO, Sentinel Chain',
            logoHeight: '29px',
            logoImage: 'sentinel@3x.png',
            logoWidth: '160px',
            name: 'Roy Lai',
            quote: 'We have been working closely with the extremely talented POA team in deploying POA’s ERC20-to-ERC20 Bridge to Sentinel Chain.<br /><br />As Sentinel is designed to be a consortium side-chain to Ethereum, the bridge will allow other ERC20 tokens, besides SENC, to be transferred between Sentinel Chain and any other EVM-based blockchains.'
        },
        {
            company: 'CEO and Co-Founder of Virtue Poker',
            logoHeight: '43px',
            logoImage: 'virtue-poker@3x.png',
            logoWidth: '160px',
            name: 'Ryan Gittleson',
            quote: 'After spending months researching various Level 2 scaling solutions, the Virtue Poker team is pleased to be partnering with the forward looking POA Network to aide in our efforts to bring decentralized poker to the globe. The use of the POA bridge in tandem with our own sidechain network enables Virtue Poker to eliminate costly on-chain Ethereum transactions in relation to hand result data. This will minimize Ethereum gas costs by only requiring main network transactions when a player wishes to enter and exit our sidechain via the POA bridge.<br /><br />This will allow the Virtue Poker platform to launch at scale, in a condensed timeframe and and more importantly at a low cost to players. We are excited to continue to work with the POA team, and are looking forward to a future with scalable Ethereum logos.'
        },
        {
            company: 'VP Blockchain, CLN Network',
            logoHeight: '42px',
            logoImage: 'cln@3x.png',
            logoWidth: '107px',
            name: 'Mark Smargon',
            quote: 'We are proud to partner with POA Network on the integration of their ERC20 bridge that links sidechains with Ethereum’s mainnet. A POA sidechain will allow us to scale our growth for our ongoing complimentary currency projects that are looking for production-ready solutions, this partnership helps us get a step closer to our goals.'
        }
    ];
    const initialSlide = 0;
    const imagesPath = 'assets/img/customer-quotes-slideshow';
    const quoteText = $('.js-quote-text');
    const customerName = $('.js-customer-name');
    const customerCompany = $('.js-customer-company');
    const companyLogos = $('.js-customer-quotes-logos');
    const companyLogosContainer = $('.js-customer-quotes-logos-container');
    const centerSlideshowActiveWidth = 767;

    let isDragging = false;
    let logosNavContainerWidth = 0;
    let logosNavDragWidth = 0;
    let allowedHorizontalDragDisplacement = 0;
    let lastX = 0;

    /**
     * Update slide data
     */
    let refreshSlideVisibleData = (index) => {

        loadQuote(index);
        setActiveLogo(index);

    };

    /**
     * Activate logo
     */
    let setActiveLogo = (index) => {

        const companyLogos = $('.js-customer-quotes-logo');

        companyLogos.removeClass('active');
        companyLogos.eq(index).addClass('active');

    };

    /**
     * Load all the company logos
     */
    let loadCompanyLogos = () => {

        const items = slideData.map((item) => {

            return  `<div
                        class="customer-quotes-logo js-customer-quotes-logo"
                        style="width: ${ item.logoWidth }; height: ${ item.logoHeight }; background-image: url(${imagesPath}/logos/${ item.logoImage });"
                    ></div>`;

        });

        companyLogos.html(items);

        $('.js-customer-quotes-logo').on('click', function() {

            const index = $(this).index();

            refreshSlideVisibleData(index);
            alignSlideshowElements($(this));

        });

    };

    /**
     * Aligns the slideshow's elements (mobile only)
     */
    let alignSlideshowElements = (element) => {

        if ($('body').width() > centerSlideshowActiveWidth) return;

        alignLogos(element);
        alignText();

    };

    /**
     * How much should we move the slider to align horizontally the selected logo.
     * @param {H} currentElementLeft
     * @param {*} targetElementLeft
     */
    let getSlideDisplacementToCenterAlignLogo = (currentElementLeft, targetElementLeft) => {

        if (currentElementLeft >= targetElementLeft) {

            return -(currentElementLeft - targetElementLeft);

        }
        else {

            return (targetElementLeft - currentElementLeft);

        }

    }

    /**
     * Align the logos horizontally
     * @param {} index
     */
    let alignLogos = (element) => {

        const screenWidth = $('body').width();
        const screenWidthHalf = parseInt(screenWidth / 2);

        let currentElementLeft = element.position().left;
        let targetElementLeft = parseInt((screenWidth - element.width()) / 2);
        let slideTargetLeftMargin = 0;

        slideTargetLeftMargin = getSliderLeftMargin() + getSlideDisplacementToCenterAlignLogo(currentElementLeft, targetElementLeft);

        /**
         * We check if the target left margin is out of bounds, and get it back
         * into them if necessary.
         */
        if (currentElementLeft >= screenWidthHalf) {

            if (slideTargetLeftMargin < allowedHorizontalDragDisplacement) {

                slideTargetLeftMargin = allowedHorizontalDragDisplacement;

            }

        }
        else {

            if (slideTargetLeftMargin > 0) {

                slideTargetLeftMargin = 0;

            }

        }

        companyLogos.animate({ marginLeft: slideTargetLeftMargin + 'px' }, 250);

    };

    /**
     * Align text
     */
    let alignText = () => {

        $("body, html").animate({ scrollTop: (companyLogosContainer.position().top - 30) + 'px' }, 250);

    };

    /**
     * Load quote text
     * @param {*} index
     */
    let loadQuote = (index) => {

        quoteText.fadeOut(125, function() {
            customerName.html(slideData[index].name);
            customerCompany.html(slideData[index].company);
            $(this).fadeIn(125).html(slideData[index].quote);
        });

    };

    /**
     * Remove quotes loading element
     * This should be asynchronous, but it'll do for now
     */
    let removeLoading = () => {

        $('.js-customer-quotes-slideshow-loading').addClass('hidden');
        $('.js-customer-quotes-slideshow-content').removeClass('hidden');

    };

    /**
     * Initialize dragging logic
     */
    let initializeDragging = () => {

        logosNavContainerWidth = companyLogosContainer .width();
        logosNavDragWidth = companyLogos.width();

        if (!isDraggable()) return;

        companyLogos.css({
            'margin-left': '0'
        });
        allowedHorizontalDragDisplacement = -Math.trunc(logosNavDragWidth - logosNavContainerWidth);

    };

    /**
     * Is draggable
     */
    let isDraggable = function() {

        return logosNavDragWidth > logosNavContainerWidth;

    };

    /**
     * Get mouse X coordinate
     * @param {*} event
     */
    let getMouseX = function(event) {

        if (event.pageX) {

            return parseInt(event.pageX);

        }

        return parseInt(event.originalEvent.touches[0].pageX);

    };

    /**
     * Can the slide be moved right?
     * @param {*} dragMargin
     */
    let canDisplaceRight = (dragMargin) => {

        return dragMargin >= 0

    }

    /**
     * Can the slide be moved left?
     * @param {*} dragMargin
     */
    let canDisplaceLeft = (dragMargin) => {

        return dragMargin <= allowedHorizontalDragDisplacement

    }

    /**
     * Get drag displacement
     */
    let getDragDisplacement = function(dragMargin, currentMouseX, _lastX) {

        // First let's check some bounds
        // Is moving right
        if (currentMouseX > _lastX) {

            if (canDisplaceRight(dragMargin)) return 0;

        }
        // Is moving left
        else if (currentMouseX < _lastX) {

            if (canDisplaceLeft(dragMargin)) return allowedHorizontalDragDisplacement;

        }

        return (dragMargin + (currentMouseX - _lastX))

    };

    /**
     * Get the slider left margin
     */
    let getSliderLeftMargin = () => {

        return parseInt(companyLogos.css('margin-left'));

    };

    /**
     * Start tabs dragging
     */
    companyLogos.on('mousedown touchstart', function(event) {

        if (!isDraggable()) return;

        isDragging = true;
        lastX = getMouseX(event);

    });

    /**
     * Is dragging
     */
    companyLogos.on('mousemove touchmove', function(event) {

        if (!isDragging) return;

        const currentMouseX = getMouseX(event);
        const dragDisplacement = getDragDisplacement(getSliderLeftMargin(), currentMouseX, lastX);

        companyLogos.css({
            'margin-left': dragDisplacement + 'px'
        });

        lastX = currentMouseX;

    });

    /**
     * Stop tabs dragging
     */
    companyLogos.on('mouseup touchend', function(event) {

        isDragging = false;

        lastX = 0

    });

    /**
     * Reset on window resizing
     */
    $(window).on('resize', function() {
        initialize();
    });

    /**
     * Init everything
     */
    let initialize = () => {

        loadQuote(initialSlide);
        loadCompanyLogos();
        setActiveLogo(initialSlide);
        removeLoading();
        initializeDragging();

    };

    initialize();

});