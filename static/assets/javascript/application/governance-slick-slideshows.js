/**
 * Governance's section slideshow
 */
$(function () {

  'use strict'

  let validatorSlider = $('.validator-slider');

  if (!validatorSlider.length) {

    return;

  }

  validatorSlider.slick({
    arrows: false,
    autoplay: true,
    autoplaySpeed: 850,
    cssEase: 'linear',
    fade: true,
    pauseOnHover: false,
    rows: 0,
    speed: 800
  });

  $('.js-dapp-block-image').slick({
    arrows: false,
    asNavFor: '.js-dapp-items',
    dots: false,
    fade: true,
    pauseOnHover: true,
    rows: 0,
    slidesToScroll: 1,
    slidesToShow: 1,
  });

  $('.js-dapp-items').slick({
    adaptiveHeight: true,
    arrows: false,
    asNavFor: '.js-dapp-block-image',
    autoplay: true,
    autoplaySpeed: 5000,
    centerMode: true,
    dots: false,
    focusOnSelect: true,
    infinite: true,
    pauseOnHover: true,
    rows: 0,
    slidesToShow: 2,
    vertical: true
  });

});