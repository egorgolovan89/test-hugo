/**
 * Filter DApps by category
 */
$(function () {
    'use strict'

    let dappsNavItem = $('.JSDappsNavItem');

    if (!dappsNavItem.length) {

        return;

    }

    const currentURL = window.location.href.substring(0, window.location.href.indexOf('?'));
    const categoryPrefix = '?category=';
    let dappsContentItems = $('.dappsContentItem');
    let isDragging = false;

    /**
     * Update URL string
     */
    const updateURLString = function(category) {

        if (typeof(window.history.pushState) === 'undefined') return;

        window.history.pushState(
            { "category":  category },
            document.title,
            currentURL + categoryPrefix + category
        );

    }

    /**
     * Get URL variables
     */
    const getUrlVars = function() {

        let vars = {};
        let parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
            vars[key] = value;
        });

        return vars;

    }

    /**
     * Check if category exists
     */
    const categoryExists = function(category) {

        return $('[data-item-category=' + category + ']').length;

    }

    /**
     * Activate tab
     * @param {*} category
     */
    const activateTab = function(category) {

        dappsNavItem.removeClass('active');
        $('[data-category-activate=' + category + ']').addClass('active');

    }

    /**
     * Show DApps by category
     */
    const showDAppsByCategory = function(category, fadeDelay = 0) {

        let dappsItems = (category == 'all') ? dappsContentItems : $('[data-item-category=' + category + ']');

        $('.dappsTabsContent').fadeOut(fadeDelay, function() {
            dappsContentItems.removeClass('visible').addClass('hidden');

            dappsItems.removeClass('hidden').addClass('visible');
            dappsItems.last().addClass('no-border');

            $('.dappsTabsContent').fadeIn(fadeDelay);

        });

    }

    /**
     * Filter by selected category
     * @param {*} category
     */
    const filterDApps = function(category, transitionEffect = false) {

        const activeCategory = (!category || !categoryExists(category)) ? 'all' : category;

        showDAppsByCategory(activeCategory, (transitionEffect ? 200 : 0));
        activateTab(activeCategory);
        updateURLString(activeCategory);

    }

    /**
     * Manage back button
     */
    window.addEventListener('popstate', function (event) {

        const category = window.history.state["category"] ? window.history.state["category"] : 'all';

        activateTab(category);
        showDAppsByCategory(category, 200);

    }, false);

    /**
     * Set active category
     */
    dappsNavItem.on("click", function() {

        if (isDragging || $(this).hasClass('active')) return;

        const category = $(this).data('category-activate') ? $(this).data('category-activate') : 'all';

        filterDApps(category, true);

    });

    /**
     * Start dragging, disable click / touch
     */
    dappsNavItem.on("mousedown touchstart", function() {

        dappsNavItem.on("mousemove touchmove",

            function() {

                isDragging = true;

            }
        );

    });

    /**
     * Finish dragging, enable click / touch
     */
    dappsNavItem.on("mouseup touchend", function() {

        isDragging = false;

    });

    /**
     * Initialize
     */
    const onLoad = function() {

        const category = getUrlVars()['category'] ? getUrlVars()['category'] : 'all';

        filterDApps(category);

    }

    onLoad();

});