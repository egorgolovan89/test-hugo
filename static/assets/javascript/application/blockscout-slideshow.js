/**
 * BlockScout slideshow
 */
$(function () {
    'use strict'

    let blockscoutSlideshow = $('#jsBlockscoutSlideshow');

    if (!blockscoutSlideshow.length) {

        return;

    }

    const imagesPath = './assets/img/blockscout/slideshow/slides/';
    const slidehowImages = {
        'ethereum': [
            'ethereum-1',
            'ethereum-2',
            'ethereum-3',
            'ethereum-4',
            'ethereum-5'
        ],
        'ethereum-classic': [
            'classic-1',
            'classic-2',
            'classic-3',
            'classic-4',
            'classic-5'
        ],
        'poa': [
            'poa-1',
            'poa-2',
            'poa-3',
            'poa-4',
            'poa-5'
        ],
        'kovan': [
            'kovan-1',
            'kovan-2',
            'kovan-3',
            'kovan-4',
            'kovan-5'
        ],
        'ropsten': [
            'ropsten-1',
            'ropsten-2',
            'ropsten-3',
            'ropsten-4',
            'ropsten-5',
        ],
        'sokol': [
            'sokol-1',
            'sokol-2',
            'sokol-3',
            'sokol-4',
            'sokol-5',
        ]
    };
    let blockscoutSlideshowGridItems = $('.js-blockscout-slideshow-grid-item');

    /**
     * Returns the image container template
     */
    const getImageTemplate = function (imagePath, imageName, imageFormat) {

        return `
            <div class="blockscout-slideshow-image">
                <div class="content-ratio">
                    <img
                        class="blockscout-slideshow-img"
                        src="${ imagePath + imageName + imageFormat }"
                        alt=""
                        srcset="${ imagePath + imageName + '@2x' + imageFormat } 2x, ${ imagePath + imageName + '@3x' + imageFormat } 3x"
                    />
                </div>
            </div>`;

    }

    /**
     * Grid item click
     */
    blockscoutSlideshowGridItems.on('click', function () {

        if ($(this).hasClass('active')) {

            return;

        }

        blockscoutSlideshowGridItems.removeClass('active');
        $(this).addClass('active');

        loadSlidehowImages($(this).data('blockscout-slideshow-activate'));

    });

    /**
     * Slideshow reset
     */
    const resetSlideshow = function () {

        $('.js-blockscout-slideshow-dots').fadeOut(0);

        blockscoutSlideshow.height($('.blockscout-slideshow-image').css('height'));

        if ($('.slick-initialized').length) {

            blockscoutSlideshow.slick('removeSlide', null, null, true);
            blockscoutSlideshow.slick('unslick');

        }

    };

    /**
     * Load slideshow images
     */
    const loadSlidehowImages = function (category) {

        const categoryImages = slidehowImages[category];

        if (!categoryImages.length) {

            console.log('No such slideshow images category...')
            return;

        }

        resetSlideshow();
        initializeSlideshow(categoryImages);

    };

    /**
     * BlockScout re initialization
     */
    blockscoutSlideshow.on('init', function (params) {

        $('.js-blockscout-slideshow-dots').fadeIn(250);
        blockscoutSlideshow.css('auto');

    });

    /**
     * Slideshow initialization
     */
    const initializeSlideshow = function (categoryImages) {

        for (let cont = 0; cont < categoryImages.length; cont++) {

            blockscoutSlideshow.append(getImageTemplate(imagesPath, categoryImages[cont], '.png'));

        }

        blockscoutSlideshow.slick({
            appendDots: $('.js-blockscout-slideshow-dots'),
            dotsClass: 'blockscout-slideshow-dot',
            arrows: false,
            autoplay: true,
            autoplaySpeed: 5000,
            dots: true,
            fade: true,
            infinite: true,
            rows: 0,
            slidesToScroll: 1,
            slidesToShow: 1,
            speed: 500,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

    };


    /**
     * Initialize
     */
    const initialize = function () {
        loadSlidehowImages($('.js-blockscout-slideshow-grid-item.active').data('blockscout-slideshow-activate'));
    };

    initialize();

});
