/**
 * Core tabs
 */
$(function () {

    "use strict";

    let topTabsContainer = $('.js-top-tabs');

    if (!topTabsContainer) {

        return;

    }

    let coreTabs = $('.js-core-top-tab');
    let coreTabsContent = $('.js-core-top-tab-content');

    /**
     * Click tab
     */
    coreTabs.on('click', function() {

        coreTabs.removeClass('active');
        $(this).addClass('active');

        coreTabsContent.removeClass('active');

        $('[data-tab-content="' + $(this).data("tab") + '"]').addClass('active');

    });

});