'use strict';
/**
 * Usage:
 *
 * 1- gulp [watch] -> bundles vendor.js, bundles and watches application/*.js, and stylesheets/.scss
 * 2- gulp build -> bundles vendor.js, application/*.js, and stylesheets/.scss ready for deployment
 *
 */

const gulp = require('gulp');

const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const compress = require('compression');
const concat = require('gulp-concat');
const gutil = require('gulp-util');
const include = require('gulp-include');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify-es').default;

/**
 * Bundle Site's SASS files.
 */
gulp.task('sass', function () {

  let stream = gulp.src('assets/stylesheets/style.scss');

  stream
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
    .pipe(autoprefixer({
      remove: false,
      browsers: [
        "last 3 versions",
        "IE 11",
        "not ie <= 10"
      ]
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('assets/stylesheets/'));

  return stream;

});

/**
 * Bundle Site's JS files.
 */
gulp.task('javascript', function () {

  let stream = gulp.src('assets/javascript/application/*.js');

  stream
    .pipe(concat('application.js'))
    .pipe(uglify())
    .on('error', onError)
    .pipe(gulp.dest('assets/javascript'));

  return stream;

});

/**
 * Bundle Vendor JS files.
 */
gulp.task('vendor', function () {

  gutil.log(gutil.colors.green('[Bundling vendors\' JS]'), '(it might take a few minutes).');

  let stream = gulp.src('assets/javascript/vendor/index.js');

  stream
    .pipe(concat('vendor.js'))
    .pipe(include())
    .pipe(uglify())
    .on('error', onError)
    .pipe(gulp.dest('assets/javascript'));

  return stream;


});

/**
 * Watch for file changes
 */
gulp.task('watch', ['vendor'], () => {

  browserSync.init({
    port: 32751,
    server: {
      baseDir: "./",
      index: "index.html",
      serveStaticOptions: {
        extensions: ["html"]
      },
      middleware: function (req, res, next) {

        let gzip = compress();

        gzip(req, res, next);

      }
    }
  });

  gulp.watch('**/*.html').on('change', browserSync.reload);
  gulp.watch('assets/stylesheets/**/*.scss', ['sass']).on('change', browserSync.reload);
  gulp.watch('assets/javascript/application/*.js', ['javascript']).on('change', browserSync.reload);

});

/**
 * Default task
 */
gulp.task('default', ['watch']);

/**
 * Build task, use for deployment.
 */
gulp.task('build', ['sass', 'vendor', 'javascript']);

/**
 * Simple helper to show errors
 * @param {} error
 */
function onError(error) {
  gutil.log(gutil.colors.red('[Error]'), error.toString());
}