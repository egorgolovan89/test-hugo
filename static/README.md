# Landing for oracles.org

# Requirements:

- node.js
- npm
- gulp

# Install

npm i

Note: You might need to delete node_modules.

# Usage

- gulp [watch] -> Opens a browser window through Browserify, bundles vendor.js, bundles and watches application/*.js, and stylesheets/.scss
- gulp build -> Bundles vendor.js, application/*.js, and stylesheets/.scss

Check gulpfile.js if you need more info about this.